import json
from io import StringIO
import random
import webbrowser
import tempfile
import os

try:
    import bs4
except:
    pass


_HERE            = os.path.dirname(os.path.abspath(__file__))
_TEST_FILEPATH   = os.path.join(_HERE, 'test.json')
_CSS_DEFAULTPATH = os.path.join(_HERE, 'style.css')

def prettify(html_code):
    """
        If we cannot use beautiful soup, do nothing.
    """
    try:
        soup = bs4.BeautifulSoup(html_code, 'html5lib')
        print('making pretty...')
        return soup.prettify()
    except:
        return html_code

class HTML_Writer:

    def __init__(self, cssfile = _CSS_DEFAULTPATH):
        self.io = StringIO()
        
        with open(cssfile) as f:
            self._css = f.read()
    
    @property
    def css(self):
        return self._css

    def println(self, *args, **kwargs):
        print(*args, file=self.io, **kwargs)

    def print(self, *args, **kwargs):
        self.println(*args, end='', **kwargs)

    def render_scalar(self, x, cls="string"):
        self.println('<span class={}>{}</span>'.format(cls, x))

    def render_num(self, x):
        self.render_scalar(x, 'num')

    def render_string(self, x):
        x = '"' + x + '"'
        self.render_scalar(x, 'string')

    def render_null(self, x):
        self.render_scalar('null', 'null')

    def render_bool(self, x):
        self.render_scalar(x, 'bool')

    def render_list(self, x):
        self.println('<ul>')
        for item in x:
            self.print('<li>')
            self.render_item(item)
            self.println('</li>')

        self.println('</ul>')

    def render_toggle(self):
        global io
        s = rand_string()
        s = """
        <input type="checkbox" unchecked id="{}"/>
        <label for="{}"></label>
        """.format(s,s)

        self.println(s)
        
    def render_object(self,x):
        global io
        self.println('<ul>')
        for key,val in x.items():
            self.print('<li>')
            if is_collapsible(val):
                self.render_toggle()
            self.render_string(key)
            self.print(': ')
            self.render_item(val)
            self.println('</li>')
        self.println('</ul>')

    def render_item(self,x):
        x = convert(x)

        if type(x) is dict:
            self.render_object(x)
        elif type(x) is list:
            self.render_list(x)
        elif type(x) is bool:
            self.render_bool(x)
        elif x is None:
            self.render_null(x)
        elif type(x) is float:
            self.render_num(x)
        else:
            self.render_string(x)
    
    def get_html(self, d):
        html_code = \
        """
        <html>
            <head>
            <style>
                {}
            </style>
            </head>
            <body>
                    {}
            </body>
        </html
        """

        self.io.seek(0)
        self.io.truncate(0)
        self.render_object(d)
        self.io.seek(0)
        s = self.io.read()

        s = html_code.format(self.css, s)

        return prettify(s)

    def write_html(self, d, filename='test.html'):
        html_code = self.get_html(d)
        with open(filename,'w+') as f:
            f.write(html_code)

    def view_html(self, d, filename = None):
        with tempfile.NamedTemporaryFile(mode='w+', delete=False, suffix='.html') as f:
            f.write(self.get_html(d))

        webbrowser.open_new(f.name)

        
def is_collapsible(val):
    if (type(val) is dict) or (type(val) is list):
        return True

    return False

def rand_string():
    return ''.join(random.choice('abcdedghijklmnopqrstuvwxyz') for i in range(20))


def convert(x):
    if type(x) is list:
        return x
    if type(x) is dict:
        return x
    
    if x is None:
        return None

    try:
        return float(x)
    except:
        pass

    if x.upper() == 'TRUE':
        return True
    
    if x.upper() == 'FALSE':
        return False
    
    if x.upper() == 'NONE' or x.upper() == 'NULL':
        return None
    
    return str(x)

class convert_json:

    def __init__(self, fun):
        self.fun = fun

    def __call__(self, d, *args, **kwargs):
        if type(d) is not dict: # assume filepath
            with open(d) as f:
                d = json.load(f)
            
        self.fun(d, *args, **kwargs)
        
@convert_json
def view(d, cssfile = _CSS_DEFAULTPATH):

    h = HTML_Writer(cssfile=cssfile)
    h.view_html(d)

@convert_json
def write(d, filename, cssfile = _CSS_DEFAULTPATH):
    h = HTML_Writer(cssfile = cssfile)
    h.write_html(d, filename)

if __name__ == '__main__':

    with open(_TEST_FILEPATH) as f:
        fulltext = f.read()
        d = json.loads(fulltext)

    h = HTML_Writer()
    h.view_html(d)

        
    